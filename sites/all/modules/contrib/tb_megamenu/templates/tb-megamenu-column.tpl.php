<div <?php print $attributes;?> class="<?php print $classes;?>">
  <div class="tb-megamenu-column-inner mega-inner clearfix">
    <?php if($close_button): ?>
      <?php print $close_button;?>
    <?php endif;?>
    <?php if($edit_button): ?>
      <?php print $edit_button;?>
    <?php endif;?>
    <?php print $tb_items;?>
  </div>
</div>
