<?php

function ym_admin($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('YourMembership API settings'),
  );
  $form['ym_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('YM API'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ym_api']['ym_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public API Key'),
    '#required' => TRUE,
    '#description' => t('The ApiKey that was provided to your organization by YourMembership.com.'),
    '#default_value' => variable_get('ym_api_key', '9FB7FC3C-0993-46DE-BA16-A991BB7012F1'),
  );
  $form['ym_api']['ym_api_key_private'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#required' => TRUE,
    '#description' => t('The Private Key that was provided to your organization by YourMembership.com.'),
    '#default_value' => variable_get('ym_api_key_private', '189E2DBA-CA25-487A-A66F-B73BDA7228B1'),
  );
  $form['ym_api']['ym_api_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Version'),
    '#required' => TRUE,
    '#description' => t('The API version to use. Default is 2.02.'),
    '#default_value' => variable_get('ym_api_version', '2.02'),
  );
  $form['ym_api']['ym_sa_passcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Sa Passcode'),
    '#required' => TRUE,
    '#description' => t('The SaPasscode that was provided to your organization by YourMembership.com.'),
    '#default_value' => variable_get('ym_sa_passcode', '6QC1QPl4aX2s'),
  );
  $form['ym_sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('YM Synchronization'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ym_sync']['ym_sync_interval'] = array(
    '#type' => 'interval',
    '#title' => t('Synchronization interval'),
    '#required' => FALSE,
    '#description' => t('Set the interval of pulling events from YM API.'),
    '#default_value' => variable_get('ym_sync_interval'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Submit function for ym_sso().
 */
function ym_admin_submit($form, &$form_state) {
  variable_set('ym_api_key', $form_state['values']['ym_api_key']);
  variable_set('ym_api_key_private', $form_state['values']['ym_api_key_private']);
  variable_set('ym_api_version', $form_state['values']['ym_api_version']);
  variable_set('ym_sa_passcode', $form_state['values']['ym_sa_passcode']);
  variable_set('ym_sync_interval', array('interval' => $form_state['values']['interval'], 'period' => $form_state['values']['period']));
  drupal_set_message(t('YourMembership API settings updated.'));
}