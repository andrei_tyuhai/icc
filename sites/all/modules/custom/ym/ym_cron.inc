<?php

/**
 * Implements hook_cron()
 */
function ym_cron() {
  // Get last sync timestamp
  $last_sync = strtotime(variable_get('ym_sync_timestamp'));

  // Get sync interval
  $date = new DateObject(0);
  $item = variable_get('ym_sync_interval');
  interval_apply_interval($date, $item);
  $interval = $date->format('U');

  // Sync if needed
  if (time() > ($last_sync + $interval)) {
    ym_events_sync($last_sync);
  }
}