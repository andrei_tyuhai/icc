<?php

function ym_sso_form($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Single Sign-On for YourMembership'),
  );
  $form['sso'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Login Form'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['sso']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#required' => TRUE,
    '#attributes' =>array('placeholder' => t('Please enter your user name at YM.')),
    '#size' => 20,
    '#maxlength' => 20,
  );
  $form['sso']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Passwod'),
    '#required' => TRUE,
    '#attributes' =>array('placeholder' => t('Please enter your password at YM.')),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Submit function for ym_sso().
 */
function ym_sso_form_submit($form, &$form_state) {
  ym_sso($form_state['values']['user'], $form_state['values']['pass']);
}