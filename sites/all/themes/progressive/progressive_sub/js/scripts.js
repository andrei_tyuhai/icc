(function ($) {
  Drupal.behaviors.addActiveClasses = {
    attach: function (context, settings) {
      $('.view-student-council .col-md-3').matchHeight();
    }
  };
})(jQuery);